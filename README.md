# BabyVAR

BabyVAR is a JAVA software for event detection in a babyfoot match.


## Contributors
Télécom Saint-Etienne group image5
<br/>[Charles Bacchis](https://www.linkedin.com/in/charles-bacchis/)
<br/>[Tomas Bonilla](https://www.linkedin.com/in/tomas-bonilla-clavijo/)
<br/>[Flavien Fechino](https://www.linkedin.com/in/flavienfechino/)
<br/>[Julien Peyrelon](https://www.linkedin.com/in/julien-peyrelon-b8580919a/)
<br/>[Ilyass Sabir](https://www.linkedin.com/in/ilyass-sabir-376a831ba/)

## Requirements
Install [java jre 16](https://www.oracle.com/java/technologies/javase/jdk16-archive-downloads.html)
<br/>Place your video files in a folder without any accent in the path

## Usage

Launch the .jar file to start the software


## Features
The software includes:
<br/>	- a cursor (slider) to shift inside the video
<br/>	- a text view and a button to go to a certain frame of the video
<br/>	- two buttons to go to the next or the last frame
<br/>	- a button to open the file we want to watch (video .mp4 for the moment)
<br/>	- a button to play, pause the video
<br/>	- a button to stop and resume the video
<br/>	- a button to full screen or normal screen the video


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

