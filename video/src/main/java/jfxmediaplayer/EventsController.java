package jfxmediaplayer;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Cursor;
import javafx.scene.control.Accordion;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.TitledPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.stage.Popup;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class EventsController {

	@FXML
	private Label score;

	private Integer scoreGauche = 0;
	private Integer scoreDroite = 0;
	private String team1 = "left";
	private String team2 = "right";
	private Controller mainController;

	public Controller getmainController() {
		return mainController;
	}

	private double processedUntil;
	@FXML
	private Accordion accordionActions;

	@FXML
	private Button saveEvents;

	@FXML
	private Button resetEvents;

	@FXML
	private TextField team1Field;

	@FXML
	private TextField team2Field;

	private List<Event> events = new ArrayList<Event>(); // 1:goal 2:exit 3:gamelle

	private DecimalFormat format = new DecimalFormat("#");

	public void setMainController(Controller mc) {
		this.mainController = mc;
	}

	// method that permit the save of the score and all actions of both teams during
	// the match (+their names)
	public void saveToFile() {
		SaveProcess propFile = mainController.getPropertiesFile();
		propFile.setProperty("score", scoreGauche.toString() + ";" + scoreDroite.toString());
		propFile.setProperty("processed_frames", Double.toString(processedUntil));
		propFile.setProperty("team1", team1Field.getText());
		propFile.setProperty("team2", team2Field.getText());
		String saveEvents = "";
		for (Event e : events) {
			saveEvents += e.toString() + ";";
		}
		propFile.setProperty("events_list", saveEvents);
		try {
			propFile.flush();
			showPopupMessage("Saved succesfully to " + propFile.getFilePath(), (Stage) score.getScene().getWindow());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// match summary human readable
	    try {
	        File myObj = new File(propFile.getFilePath().substring(0, propFile.getFilePath().length() - 11) + ".txt");
	        myObj.createNewFile(); // if it doesn't exist
	        FileWriter myWriter = new FileWriter(myObj);
	        String fileContent = "Score: " + team1 + ": " + score.getText() + " :" + team2;
	        for (Event e:events) {
	        	if (e.getAction_() == 1) {
		        	fileContent += "\nTeam " + e.getTeam_() + " scored at "
							+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
							+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60);
	        	}
	        	if (e.getAction_() == 2) {
	        		fileContent += "\nTeam " + e.getTeam_() + " threw the ball out of play at "
							+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
							+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60);
	        	}
	        	if (e.getAction_() == 3) {
	        		fileContent += "\nGamelle by team " + e.getTeam_() + " at "
							+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
							+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60);
	        	}
	        }
	        myWriter.write(fileContent);
	        myWriter.close();
	      } catch (IOException e) {
	        System.out.println("An error occurred.");
	        e.printStackTrace();
	      }
	}

	// reset the whole pane of events and team names
	public void resetEvents() {
		team1Field.setDisable(false);
		team2Field.setDisable(false);
		scoreGauche = 0;
		scoreDroite = 0;
		processedUntil = 0.;
		team1 = "gauche";
		team2 = "droite";
		events = new ArrayList<Event>();
		try {
			updateDisplay();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	// getter and setter for the score on the pane (updatedisplay())
	public Label getScore() {
		return score;
	}

	public void setScore(Integer sc1, Integer sc2) throws InterruptedException {
		this.scoreGauche = sc1;
		this.scoreDroite = sc2;
		updateDisplay();
	}

	// recover events that are going on
	public List<Event> getEvents() {
		return events;
	}

	public void setEvents(List<Event> events) throws InterruptedException {
		this.events = events;
		updateDisplay();
	}

	// setter for the process of events
	public void setProccessed(double processedUntil) {
		this.processedUntil = processedUntil;
	}

	// set the names of both teams
	public void setTeamNames(String team1, int side) {
		if (side == 1) {
			this.team1 = team1;
			team1Field.setText(team1);
		} else {
			this.team2 = team1;
			team2Field.setText(team1);
		}
	}

	/**
	 * method that we add all types of events to our event stage window This show :
	 * the score the actions of the game buttons to save results and stop analysis
	 * of the match buttons to remove actions manually labels with time stamps and
	 * functional go back to the according action possibility to custom your team
	 * name for the whole match
	 */
	public void addEvent(int frame, int event, int side) throws InterruptedException {
		team1Field.setDisable(true);
		team2Field.setDisable(true);
		team1 = team1Field.getText();
		team2 = team2Field.getText();
		if (processedUntil < frame) {
			processedUntil = frame + 1;
			if (event == 1) {
				if (events.size() > 0) {
					Event lastEvent = events.get(events.size() - 1);
					if ((frame - lastEvent.getFrame_()) < 20) { // if we detect a goal before the gamelle we simply
																// delete the goal after all
						System.out.println(lastEvent);
						if (lastEvent.getAction_() == 1) {
							if (lastEvent.getTeam_().equals(team1)) {

								scoreGauche--;
							} else {
								scoreDroite--;
							}
						}
						if (lastEvent.getAction_() == 3) {
							if (lastEvent.getTeam_().equals(team1)) {
								scoreGauche--;
								scoreDroite++;
							} else {
								scoreDroite--;
								scoreGauche++;
							}
						}
						events.remove(events.size() - 1);
					}
				}
				if (side == 0) {
					scoreGauche++;
					events.add(new Event(1, frame, team1));
				} else {
					scoreDroite++;
					events.add(new Event(1, frame, team2));
				}
			}
			if (event == 3) {
				if (events.size() > 0) {
					Event lastEvent = events.get(events.size() - 1);
					if ((frame - lastEvent.getFrame_()) < 20 && lastEvent.getAction_() == 1) { // if we detect a goal
																								// before the gamelle we
																								// simply delete the
																								// goal after all
						events.remove(events.size() - 1);
						if (lastEvent.getTeam_().equals(team1)) {
							scoreGauche--;
						} else {
							scoreDroite--;
						}
					}
				}
				if (side == 0) {
					scoreGauche++;
					if (scoreDroite > 0) {
						scoreDroite--;
					}
					events.add(new Event(3, frame, team1));
				} else {
					scoreDroite++;
					if (scoreGauche > 0) {
						scoreGauche--;
					}
					events.add(new Event(3, frame, team2));
				}
			}
			updateDisplay();
		}
	}

	// For the fxml add buttons (has to skip processedUntil verification)
	// handle goals on the middle right part
	public void addLeft() throws InterruptedException {
		team1Field.setDisable(true);
		team2Field.setDisable(true);
		team1 = team1Field.getText();
		team2 = team2Field.getText();
		int frame = (int) mainController.getVideo().getFrame();
		scoreGauche++;
		events.add(new Event(1, frame, team1));
		updateDisplay();
	}

	// handle goals on the middle left part
	public void addRight() throws InterruptedException {
		team1Field.setDisable(true);
		team2Field.setDisable(true);
		team1 = team1Field.getText();
		team2 = team2Field.getText();
		int frame = (int) mainController.getVideo().getFrame();
		scoreDroite++;
		events.add(new Event(1, frame, team2));
		updateDisplay();
	}

	// handle exit moves
	public void addSortie(int frame, int side) throws InterruptedException {
		team1Field.setDisable(true);
		team2Field.setDisable(true);
		team1 = team1Field.getText();
		team2 = team2Field.getText();
		if (processedUntil < frame) {
			processedUntil = frame + 1;
			if (side == 0) {
				events.add(new Event(2, frame, team1));
			} else {
				events.add(new Event(2, frame, team2));
			}
			updateDisplay();
		}
	}

	public void updateDisplay() throws InterruptedException {
		// label that keep the score updated
		mainController.setScore(scoreGauche, scoreDroite);
		score.setText(scoreGauche.toString() + " - " + scoreDroite.toString());
		// all the actions on a pane
		accordionActions.getPanes().removeAll(accordionActions.getPanes());
		for (Event e : events) {

			HBox hbox = new HBox();

			// button that handle the deletion of the action
			Button deleteAction = new Button();

			deleteAction.setText("delete");
			deleteAction.setOnMouseClicked(new EventHandler<MouseEvent>() {
				@Override
				public void handle(MouseEvent event) {
					if (e.getAction_() == 1) {
						if (e.getTeam_() == team1) {
							scoreGauche--;
						} else {
							scoreDroite--;
						}
					}
					if (e.getAction_() == 3) {
						if (e.getTeam_().equals(team1)) {
							scoreGauche--;
							if (scoreDroite > 0) { // if there is gamelle but the team has already 0 we do not count it
													// as a gamelle, juste like a goal
								scoreDroite++;
							}
						} else {
							if (scoreGauche > 0) {
								scoreGauche++;
							}
							scoreDroite--;
						}
					}
					events.remove(e);
					try {
						updateDisplay();
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}

			});
			hbox.getChildren().add(deleteAction);

			// text label to see the timestamp of the action and go back to it
			Label timeStamp = new Label(
					"L'action s'est déroulée à : " + format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60)
							+ ":" + format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60));
			// fxml styling
			timeStamp.setCursor(Cursor.HAND);
			timeStamp.setAlignment(Pos.CENTER);
			timeStamp.setStyle("-fx-padding: 3 0 0 20;");
			timeStamp.setOnMouseClicked(new EventHandler<MouseEvent>() {
				// if we click on the label of an action, we go back to it
				@Override
				public void handle(MouseEvent event) {
					mainController.getVideo().seek(e.getFrame_() - 3 * Controller.fps); // rewind 3 seconds before the
																						// action

				}
			});
			hbox.getChildren().add(timeStamp);

			// Add the titled pane according to the action type -goal -gamelle
			TitledPane pane = new TitledPane();
			if (e.getAction_() == 1) {
				pane = new TitledPane("Goal by team " + e.getTeam_(), hbox);
				timeStamp.setText("Team " + e.getTeam_() + " scored at "
						+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
						+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60));
			}
			if (e.getAction_() == 2) {
				pane = new TitledPane("Ball out of play by team " + e.getTeam_(), hbox);
				timeStamp.setText("Team " + e.getTeam_() + " threw the ball out of play at "
						+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
						+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60));
			}
			if (e.getAction_() == 3) {
				pane = new TitledPane("Gamelle by team " + e.getTeam_(), hbox);
				timeStamp.setText("Gamelle by team " + e.getTeam_() + " at "
						+ format.format(((double) e.getFrame_() * 1 / Controller.fps) / 60) + ":"
						+ format.format((double) (e.getFrame_() * 1 / Controller.fps) % 60));
			}

			accordionActions.getPanes().add(pane);
		}
	}
	
	// https://stackoverflow.com/a/18687993
	public static Popup createPopup(final String message) {
	    final Popup popup = new Popup();
	    popup.setAutoFix(true);
	    popup.setAutoHide(true);
	    popup.setHideOnEscape(true);
	    Label label = new Label(message);
	    label.setOnMouseReleased(new EventHandler<MouseEvent>() {
	        @Override
	        public void handle(MouseEvent e) {
	            popup.hide();
	        }
	    });
	    label.setStyle("-fx-background-color: white;"
	    		+ "-fx-padding: 10;"
	    		+ "-fx-border-color: grey; "
	    		+ "fx-border-width: 2;"
	    		+ "-fx-font-size: 14;");
	    label.getStyleClass().add("popup");
	    popup.getContent().add(label);
	    return popup;
	}

	public static void showPopupMessage(final String message, final Stage stage) {
	    final Popup popup = createPopup(message);
	    popup.setOnShown(new EventHandler<WindowEvent>() {
	        @Override
	        public void handle(WindowEvent e) {
	            popup.setX(stage.getX() + stage.getWidth()/2 - popup.getWidth()/2);
	            popup.setY(stage.getY() + stage.getHeight()/2 - popup.getHeight()/2);
	        }
	    });        
	    popup.show(stage);
	}
}
