package jfxmediaplayer;

import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;

import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioMenuItem;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import java.io.File;
import java.io.IOException;
import java.net.URL;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

public class Controller implements Initializable {
	// The main container of the application that holds everything.
	@FXML
	private VBox vboxParent;

	// Media objects used to display and work with the video.
	@FXML
	private ImageView currentFrame;

	ImageView getFrame() {
		return currentFrame;
	}

	// The label that displays the current score in the main window
	@FXML
	private Label scoreLabel;

	void setScore(Integer sc1, Integer sc2) {
		scoreLabel.setText(sc1.toString() + " - " + sc2.toString());
	}

	// the OpenCV object that realizes the video capture
	private OpenCVVideo video;

	OpenCVVideo getVideo() {
		return video;
	}

	// The HBox that contains the controls under the video.
	@FXML
	private HBox hBoxControls;

	// The button that plays, pauses, and restarts.
	@FXML
	private Label buttonPPR;

	private boolean nightModeOn = false;

	@FXML
	private Button buttonActions;
	// Labels that allow us to go 1 image back or forward
	@FXML
	private Label button1ImgBack;
	@FXML
	private Label button1ImgFwd;

	// Label to let the user draw back of nets
	@FXML
	private Label buttonDraw;
	// Labels that are used to display the current and total time.
	@FXML
	private Label labelCurrentTime;
	@FXML
	private Label labelTotalTime;

	// Our BorderPane where we display all the app interface
	@FXML
	private BorderPane borderPane;

	// Labels that are used to display the current and total frames.
	@FXML
	private Label labelCurrentFrame;
	@FXML
	private Label labelTotalFrame;

	// Label that makes the application full screen.
	@FXML
	private Label labelFullScreen;

	// The slider used to change the volume.
	// @FXML
	// private Slider sliderVolume;
	// Slider that lets you control and tracks the current time of the video.
	@FXML
	private Slider sliderTime;

	// The menubar and its items
	@FXML
	private MenuItem helpWindow;
	@FXML
	private MenuItem About;
	@FXML
	private MenuItem openFile;
	@FXML
	private ColorPicker colorPicker;
	@FXML
	private MenuItem darkMode;
	@FXML
	private RadioMenuItem pauseAnalysis;
	@FXML
	private MenuItem exit;

	// Checks if the video is at the end.
	private boolean atEndOfVideo = false;
	// Video is not playing when GUI starts.
	private boolean isPlaying = false;
	// Terrain limits are being set
	private boolean isMaskVisible = true;

	// ImageViews for the buttons and labels.
	private ImageView ivPlay;
	private ImageView ivPlus1;
	private ImageView ivMinus1;
	private ImageView ivPause;
	private ImageView ivRestart;
	private ImageView ivFullScreen;
	private ImageView ivExit;
	private ImageView ivDraw;
	private ImageView ivCheck;

	// Frame per second value and its format to avoid too much precision
	private DecimalFormat format = new DecimalFormat("0.0#");
	private DecimalFormat format2 = new DecimalFormat("#");

	// GUI to go to a specified frame
	@FXML
	private TextField frameNumber;

	@FXML
	private Button buttonToFrame;

	// Events window Controller and stage
	private EventsController eventsController;
	private Stage eventsStage;

	private Stage helpStage;

	// The masks the user will place to match the video
	public MovableMask terrain = new MovableMask(72, 16, 400, 248, Color.rgb(255, 0, 0, 0.2));
	public MovableMask goal1 = new MovableMask(72, 108, 24, 64, Color.rgb(0, 255, 0, 0.2));
	public MovableMask goal2 = new MovableMask(448, 108, 24, 64, Color.rgb(0, 255, 0, 0.2));

	private SaveProcess properties;

	static double fps = 60;

	public void pauseVid() {
		buttonPPR.setGraphic(ivPlay);
		try {
			video.pause();
		} catch (InterruptedException e) {
			System.err.println("Can't pause video in Controller.java" + e);
		}
		// The video is now paused so change it to false.
		isPlaying = false;
	}

	public void playVid() {
		// The video was paused so when the button is clicked change the image to stop
		// and play video.
		buttonPPR.setGraphic(ivPause);
		video.play();
		// The video is now playing so isPlaying is true.
		isPlaying = true;

	}

	@Override
	public void initialize(URL url, ResourceBundle resourceBundle) {
		this.currentFrame.setImage(new Image(getClass().getResource("fondImage.png").toString()));
		
		this.colorPicker.setValue(Color.YELLOW);
		
		// Get the paths of the images and make them into images.
		Image imagePlay = new Image(getClass().getResource("play-btn.png").toString());
		ivPlay = new ImageView(imagePlay);
		ivPlay.setFitWidth(35);
		ivPlay.setFitHeight(35);

		Image imagePlus1 = new Image(getClass().getResource("plus1-btn.png").toString());
		ivPlus1 = new ImageView(imagePlus1);
		ivPlus1.setFitWidth(35);
		ivPlus1.setFitHeight(35);

		Image imageMinus1 = new Image(getClass().getResource("minus1-btn.png").toString());
		ivMinus1 = new ImageView(imageMinus1);
		ivMinus1.setFitWidth(35);
		ivMinus1.setFitHeight(35);

		// Button stop image.
		Image imageStop = new Image(getClass().getResource("stop-btn.png").toString());
		ivPause = new ImageView(imageStop);
		ivPause.setFitHeight(35);
		ivPause.setFitWidth(35);

		// Restart button image.
		Image imageRestart = new Image(getClass().getResource("restart-btn.png").toString());
		ivRestart = new ImageView(imageRestart);
		ivRestart.setFitWidth(35);
		ivRestart.setFitHeight(35);

		// Full screen image.
		Image imageFull = new Image(getClass().getResource("fullscreen.png").toString());
		ivFullScreen = new ImageView(imageFull);
		ivFullScreen.setFitHeight(35);
		ivFullScreen.setFitWidth(35);

		// Exit full screen image.
		Image imageExit = new Image(getClass().getResource("exitscreen.png").toString());
		ivExit = new ImageView(imageExit);
		ivExit.setFitHeight(35);
		ivExit.setFitWidth(35);

		// Show / Change masks position
		Image imagePencil = new Image(getClass().getResource("pencil.png").toString());
		ivDraw = new ImageView(imagePencil);
		ivDraw.setFitHeight(35);
		ivDraw.setFitWidth(35);

		// Hide masks position / handles
		Image imageCheck = new Image(getClass().getResource("check.png").toString());
		ivCheck = new ImageView(imageCheck);
		ivCheck.setFitHeight(35);
		ivCheck.setFitWidth(35);

		// Sets the graphics for each label
		buttonDraw.setGraphic(ivCheck);
		buttonPPR.setGraphic(ivPlay);
		labelFullScreen.setGraphic(ivFullScreen);
		button1ImgBack.setGraphic(ivMinus1);
		button1ImgFwd.setGraphic(ivPlus1);

		// We don't want the user to check a button that won't change the toggled value
		// inside OpenCVVideo
		pauseAnalysis.setDisable(true);
		helpWindow.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				helpStage.getIcons().add(new Image(getClass().getResource("icon-help.png").toString()));
				helpStage.show();
			}

		});

		// Change colors to reduce eye strain
		darkMode.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				if (nightModeOn) {
					nightModeOn = false;
					borderPane.setBackground(new Background(
							new BackgroundFill(new Color(0.2, 0.2, 0.2, 0.1), CornerRadii.EMPTY, Insets.EMPTY)));
					scoreLabel.setTextFill(new Color(0.2, 0.2, 0.2, 1));
					labelCurrentTime.setTextFill(new Color(0.2, 0.2, 0.2, 1));
					labelTotalTime.setTextFill(new Color(0.2, 0.2, 0.2, 1));
					labelCurrentFrame.setTextFill(new Color(0.2, 0.2, 0.2, 1));
					labelTotalFrame.setTextFill(new Color(0.2, 0.2, 0.2, 1));
				} else {
					nightModeOn = true;
					borderPane.setBackground(new Background(
							new BackgroundFill(new Color(0.2, 0.2, 0.2, 1), CornerRadii.EMPTY, Insets.EMPTY)));
					scoreLabel.setTextFill(Color.LIGHTBLUE);
					labelCurrentTime.setTextFill(Color.LIGHTBLUE);
					labelTotalTime.setTextFill(Color.LIGHTBLUE);
					labelCurrentFrame.setTextFill(Color.LIGHTBLUE);
					labelTotalFrame.setTextFill(Color.LIGHTBLUE);
				}
			}
		});

		openFile.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				// If a previous video was loaded
				if (video != null) {
					video.getContainer().release();
				}
				initializer();
			}

		});

		// Close the program
		exit.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				Stage stage = (Stage) buttonPPR.getScene().getWindow();
				stage.close(); // To use the setOnCloseRequest function in Setup.java that handles window close
			}
		});
	}

	public void initializer() {
		Scene scene = buttonPPR.getScene();
		Stage stage = (Stage) scene.getWindow();
		FileChooser fileChooser = new FileChooser();
		// Only allow to choose the correct file extensions
		FileChooser.ExtensionFilter filter = new FileChooser.ExtensionFilter("Select a file(*.mp4) ", "*.mp4", "*.MP4");
		fileChooser.getExtensionFilters().add(filter);

		File file = fileChooser.showOpenDialog(null);

		if (file != null) { // If the user chose a file
			// Prepare the Events window
			FXMLLoader eventsLoader = new FXMLLoader(getClass().getResource("events.fxml"));
			try {

				eventsStage = new Stage();
				eventsStage.setScene(new Scene(eventsLoader.load(), 600, 500));

				eventsStage.setResizable(false);
				eventsStage.setTitle("Actions of the match");
			} catch (IOException e1) {
				System.err.println("Couldn't initialize the events window in Controler.java " + e1);
			}
			eventsController = eventsLoader.getController();
			eventsController.setMainController(this);
			// Open video with openCV
			video = new OpenCVVideo(currentFrame, file.toString(), goal1, goal2, terrain, eventsController, sliderTime,
					labelCurrentFrame, labelCurrentTime);

			sliderTime.setMax(video.getTotalFrame());
			// The height offset induced by the time labels on the slider is too much
			VBox.setMargin(sliderTime, new Insets(0, 10, -7, 10));
			Controller.fps = video.getFPS();
			labelTotalTime.setText((format2.format((video.getTotalFrame() * 1 / fps) / 60) + ":"
					+ format2.format((video.getTotalFrame() * 1 / fps) % 60)) + " mm:ss");
			labelTotalFrame.setText(format.format(video.getTotalFrame()) + " frames");
		} else {
			// No file was chosen
			return;
		}
		// Reset score (if new video is loaded)
		eventsController.resetEvents();
		// Loading the save file for that video
		properties = new SaveProcess(video.getTruncatedPath());
		Set<String> pptNames = properties.getAllPropertyNames();
		for (String s : pptNames) {
			switch (s) {
			case "score":
				String score = properties.getProperty(s);
				String[] scores = score.split(";");
				try {
					eventsController.setScore(Integer.parseInt(scores[0]), Integer.parseInt(scores[1]));
				} catch (NumberFormatException e1) {
					e1.printStackTrace();
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				break;
			case "processed_frames":
				eventsController.setProccessed(Double.parseDouble(properties.getProperty(s)));
				break;
			case "events_list":
				String[] events = properties.getProperty(s).split(";");

				if (events.length > 0) {
					List<Event> eventsL = new ArrayList<Event>();
					for (String sEv : events) {
						String[] eventStr = sEv.split(",");
						if (eventStr[0].length()>0 && eventStr[1].length()>0 && eventStr[2].length()>0) {
							Event e = new Event(Integer.parseInt(eventStr[0]), Integer.parseInt(eventStr[1]), eventStr[2]);
							eventsL.add(e);
						}
					}

					try {
						eventsController.setEvents(eventsL);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				break;
			case "team1":
				eventsController.setTeamNames(properties.getProperty(s), 1);
				break;
			case "team2":
				eventsController.setTeamNames(properties.getProperty(s), 2);
				break;
			}

		}
		// Time labels in frames
		sliderTime.setShowTickLabels(true);
		sliderTime.setShowTickMarks(true);
		sliderTime.setMajorTickUnit(video.getTotalFrame() / 10);
		sliderTime.setMinorTickCount((int) (video.getTotalFrame() / 20));
		setButtonsListeners(scene, stage);

		// Don't Play the video when the application starts.
		currentFrame.setPreserveRatio(true);

		// Bindings to fit the video in the window at all sizes
		currentFrame.fitHeightProperty().bind(scene.heightProperty().subtract(125));
		currentFrame.fitWidthProperty().bind(scene.widthProperty());

		// Remove the key right / left to scroll through (we add our own implementation
		// after)
		sliderTime.setBlockIncrement(0);

		// Method for updating the time and frame labels
		sliderTime.valueChangingProperty().addListener(new ChangeListener<Boolean>() {
			@Override
			public void changed(ObservableValue<? extends Boolean> observableValue, Boolean wasChanging,
					Boolean isChanging) {
				bindCurrentTimeAndFrameLabel();
				if (!isChanging) {
					// video.seek(format.parse(format.format(sliderTime.getValue())).doubleValue());
					labelCurrentTime.setText(format.format(sliderTime.getValue()));
					labelCurrentFrame.setText(format2.format((sliderTime.getValue() * 1 / fps) / 60) + ":"
							+ format2.format((sliderTime.getValue() * 1 / fps) % 60) + " /");
				}
			}
		});

		// Method for updating correctly the sliderTime of the opencvvideo
		sliderTime.valueProperty().addListener(new ChangeListener<Number>() {
			@Override
			public void changed(ObservableValue<? extends Number> observableValue, Number oldValue, Number newValue) {
				bindCurrentTimeAndFrameLabel();
				double currentFrame = video.getFrame();
				if (Math.abs(currentFrame - newValue.doubleValue()) > 0.5) {
					video.seek(newValue.doubleValue());
					playVid();
				}
				labelsMatchEndVideo(labelCurrentTime.getText(), labelTotalTime.getText());
			}
		});

		// Initialize the masks postion at the end to have the final video size (less
		// resizing)
		terrain.init(currentFrame);
		goal1.init(currentFrame);
		goal2.init(currentFrame);
	}

	public void updateSliderTimer() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				if (!sliderTime.isValueChanging()) {
					sliderTime.setValue(video.getFrame() * video.getFPS());
				}
			}

		});
	}

	private void setButtonsListeners(Scene scene, Stage stage) {
		// Double click the video to fullscreen
		currentFrame.setOnMouseClicked(new EventHandler<MouseEvent>() {

			@Override
			public void handle(MouseEvent event) {
				if (event.getClickCount() == 2) {
					stage.setFullScreen(true);
				}
			}

		});
		// Open Events window
		buttonActions.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				eventsStage.getIcons().add(new Image(getClass().getResource("icon-score.png").toString()));
				eventsStage.show();
			}
		});

		// Open color picker to choose the color that is used to analyse the video
		colorPicker.valueProperty().addListener(new ChangeListener<Color>() {
			@Override
			public void changed(@SuppressWarnings("rawtypes") ObservableValue ov, Color t, Color t1) {

				Platform.runLater(new Runnable() {
					@Override
					public void run() {
						double h = 0;
						double hmin = 0;
						double hmax = 0;
						h = Math.floor((colorPicker.getValue().getHue() / 360) * 179);
						// System.out.println(h+" "+hmin+" "+hmax);
						hmin = h - 5;
						hmax = h + 5;
						video.changeBallColor(hmin, hmax);
					}
				});

			}
		});

		buttonDraw.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// Implement drawing on video to place goals / terrain
				terrain.toggleVisibility();
				goal1.toggleVisibility();
				goal2.toggleVisibility();
				if (isMaskVisible) {
					buttonDraw.setGraphic(ivDraw);
					if (!atEndOfVideo   && !isPlaying) {
						playVid();
					}
					isMaskVisible = false;
				} else {
					// The video was paused so when the button is clicked change the image to stop
					// and play video.
					buttonDraw.setGraphic(ivCheck);
					if (!atEndOfVideo && isPlaying) {
						pauseVid();
					}
					isMaskVisible = true;
				}
			}
		});

		// play / pause
		buttonPPR.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// Get the button that is clicked from the event.
				bindCurrentTimeAndFrameLabel();
				// If it is the end of the video then reset the slider to 0 and restart the
				// video.
				if (atEndOfVideo) {
					sliderTime.setValue(0);
					atEndOfVideo = false;
					isPlaying = false;
				}
				// If the video is playing and the button is clicked pause the video and change
				// the image on the button to play.
				if (isPlaying) {
					pauseVid();
				} else {
					playVid();
				}
			}
		});

		// Seek video to frame entered in TextField
		buttonToFrame.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (!(frameNumber.getText().equals("-1") && isPlaying)) {
					video.seek(Double.parseDouble(frameNumber.getText()));
				}
			}
		});

		// FullScreen mode or not handler
		labelFullScreen.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if (stage.isFullScreen()) {
					stage.setFullScreen(false);
					labelFullScreen.setGraphic(ivFullScreen);
				} else {
					stage.setFullScreen(true);
					labelFullScreen.setGraphic(ivExit);
					stage.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler<KeyEvent>() {
						@Override
						public void handle(KeyEvent keyEvent) {
							if (keyEvent.getCode() == KeyCode.ESCAPE) {
								labelFullScreen.setGraphic(ivFullScreen);
							}
						}
					});
				}
			}
		});
		// Method handling the -1 image per button press
		button1ImgBack.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if ((video.getFrame()) != 0) {

					video.seek(video.getFrame() - 1);

				}
			}
		});

		// Method handling the +1 image per button press
		button1ImgFwd.setOnMouseClicked(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				if ((video.getFrame()) != (video.getTotalFrame() - 1)) {
					video.seek(video.getFrame() + 1);
				}

			}
		});

		// Handle keybinds
		scene.addEventFilter(KeyEvent.KEY_PRESSED, keyEvent -> {
			if (keyEvent.getCode() == KeyCode.SPACE) {
				if (isPlaying) {
					pauseVid();
				} else {
					playVid();
				}
			}
			if (keyEvent.getCode() == KeyCode.LEFT) {
				video.seek(video.getFrame() - 1);
				// mpVideo.seek(Duration.seconds(mpVideo.getCurrentTime().toSeconds()-1/fps));
			}
			if (keyEvent.getCode() == KeyCode.RIGHT) {
				video.seek(video.getFrame() + 1);
				// mpVideo.seek(Duration.seconds(mpVideo.getCurrentTime().toSeconds()-1/fps));
			}
		});

		// The OpenCVVideo class is loaded so we can enable the radio button
		pauseAnalysis.setDisable(false);
		pauseAnalysis.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent event) {
				video.toggleAnalysis();
			}
		});
	}

	public SaveProcess getPropertiesFile() {
		return properties;
	}

	// Test where we are : end, middle or start, updating the button
	public void labelsMatchEndVideo(String labelTime, String labelTotalTime) {
		for (int i = 0; i < labelTotalTime.length(); i++) {
			if (labelTime.charAt(i) != labelTotalTime.charAt(i)) {
				atEndOfVideo = false;
				if (isPlaying)
					buttonPPR.setGraphic(ivPause);
				else
					buttonPPR.setGraphic(ivPlay);
				break;
			} else {
				atEndOfVideo = true;
				buttonPPR.setGraphic(ivRestart);
			}
		}
	}

	/**
	 * Updating frame label and time label source :
	 * https://github.com/scalafx/scalafx/issues/232,
	 * https://stackoverflow.com/questions/38543273/javafx-bindings-createstringbinding-but-binding-not-actually-work
	 */
	public void bindCurrentTimeAndFrameLabel() {
		labelCurrentTime.setText(format.format(video.getFrame() / video.getFPS()) + " /");

		labelCurrentFrame.setText(format.format(video.getFrame()) + " /");
	}

	public void setHelpStage(Stage stage) {
		helpStage = stage;
	}
}
