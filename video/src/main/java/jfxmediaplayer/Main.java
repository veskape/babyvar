package jfxmediaplayer;

// We use a second main that does not extend any third class (e.j: Application).
// This would create a thread bug using any OpenCV library
public class Main {

	public static void main(String[] args) {
		Setup.main(args);
	}

}
