package jfxmediaplayer;

import java.util.ArrayList;
import java.util.List;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.DecimalFormat;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfPoint;
import org.opencv.core.Point;
import org.opencv.core.Rect;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;
import org.opencv.videoio.VideoCapture;
import org.opencv.videoio.Videoio;

import javafx.application.Platform;
import javafx.scene.control.Label;
import javafx.scene.control.Slider;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

public class OpenCVVideo {

	// a timer for acquiring the video stream
	private ScheduledExecutorService timer;
	private ScheduledFuture<?> future;
	// the OpenCV object that realizes the video capture
	private VideoCapture capture = new VideoCapture();

	public VideoCapture getContainer() {
		return capture;
	}
	
	// divide the resolution of the video by 1/2
	Double resDivider = 1.5;

	private TimerTask frameGrabber;

	private Mat hsv_image = new Mat();
	private Mat thresholded = new Mat();
	private Mat thresholded2 = new Mat();
	private Mat circles = new Mat();
	private Mat array255;
	private Mat distance;

	// list of hsv profils
	private List<Mat> lhsv = new ArrayList<Mat>(3);

	// hsv scalars for segmentation
	private Scalar hsv_min = new Scalar(22, 50, 50, 0);
	private Scalar hsv_max = new Scalar(38, 255, 255, 0);
	private Scalar hsv_min2 = new Scalar(175, 50, 50, 0);
	private Scalar hsv_max2 = new Scalar(179, 255, 255, 0);

	// size and fps of the video
	private long fps;
	private Size sz;

	// the 3 movable mask that define the two goals and the whole playground
	private MovableMask goal1;
	private MovableMask goal2;
	private MovableMask terrain;

	// variables that handle the goal detection
	private boolean inAGoal = false;
	private boolean fieldExit = true; // We wait that the ball enters the playground to start detecting the exits
	private EventsController eventsController;
	private double videoWidth;
	public double getWidth() {return videoWidth;}
	private double videoHeight;
	public double getHeight() {return videoHeight;}

	// a flag to change the button behavior
	private boolean playing = false;
	private Path path;

	// stop the detection : frame analysis
	private Boolean pauseAnalysis = false;
	private Slider slider;
	private Label currentframetext;
	private Label currenttimetext;

	// format to have only one decimal
	private DecimalFormat format = new DecimalFormat("#");

	// save the goal frame to animate a specific time the goal
	private Integer animBut1 = -1;
	private Integer animBut2 = -1;

	// save the last move in a list
	private List<Boolean> lastMvm = new ArrayList<>();
	private List<Boolean> lastMvm2 = new ArrayList<>();

	// constructor of the class
	OpenCVVideo(ImageView currentFrame, String path, MovableMask goal1, MovableMask goal2, MovableMask terrain,
			EventsController controller, Slider slider_, Label currentframetext_, Label currenttimetext_) {
		// init all our attributes by reference
		super();
		this.slider = slider_;
		this.currentframetext = currentframetext_;
		this.currenttimetext = currenttimetext_;
		this.path = Paths.get(path);
		this.goal1 = goal1;
		this.goal2 = goal2;
		this.terrain = terrain;
		this.eventsController = controller;


		if (!this.playing) {

			// start the video capture
			this.capture.open(path);

			// init size and fps values
			fps = (long) capture.get(Videoio.CAP_PROP_FPS);
			videoWidth = capture.get(Videoio.CAP_PROP_FRAME_WIDTH);
			videoHeight = capture.get(Videoio.CAP_PROP_FRAME_HEIGHT);

			// is the video stream available?
			if (this.capture.isOpened()) {
				sz = new Size((int) capture.get(3) / resDivider, (int) capture.get(4) / resDivider);
				this.array255 = new Mat(sz, CvType.CV_8UC1);
				this.distance = new Mat(sz, CvType.CV_8UC1);
				this.array255.setTo(new Scalar(255));
				this.playing = true;
				frameGrabber = new TimerTask() {

					@Override
					public synchronized void run() {
						// effectively grab and process a single frame
						Mat frame = grabFrame();
						// convert and show the frame
						if (!frame.empty()) {
							Image imageToShow = Utils.mat2Image(frame);
							updateImageView(currentFrame, imageToShow);
						} else {
							try {
								pause();
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					}
				};
				this.timer = Executors.newSingleThreadScheduledExecutor();

				// Show the first frame of the video but paused
				frameGrabber.run();
			} else {
				// log the error
				System.err.println("Impossible to open the video file.");
			}
		} else {
			this.playing = false;

			// stop the timer
			this.stopAcquisition();
		}
	}

	private synchronized Mat grabFrame() {
		Mat frame = new Mat();

		// check if the capture is open
		if (this.capture.isOpened()) {
			try {
				// read the current frame
				this.capture.read(frame);

				// if the frame is not empty, process it
				if (!frame.empty() && !pauseAnalysis) {
					Imgproc.resize(frame, frame, sz);
					Imgproc.cvtColor(frame, hsv_image, Imgproc.COLOR_BGR2HSV);
					Core.inRange(hsv_image, hsv_min, hsv_max, thresholded);
					Core.inRange(hsv_image, hsv_min2, hsv_max2, thresholded2);
					Core.bitwise_or(thresholded, thresholded2, thresholded);
					Core.split(hsv_image, lhsv);
					Mat S = lhsv.get(1);
					Mat V = lhsv.get(2);

					Core.subtract(array255, S, S);
					Core.subtract(array255, V, V);

					S.convertTo(S, CvType.CV_32F);
					V.convertTo(V, CvType.CV_32F);

					Core.magnitude(S, V, distance);

					Core.inRange(distance, new Scalar(0.0), new Scalar(200.0), thresholded2);

					Core.bitwise_and(thresholded, thresholded2, thresholded);
					Imgproc.GaussianBlur(thresholded, thresholded, new Size(9, 9), 0, 0);
					Imgproc.HoughCircles(thresholded, circles, Imgproc.CV_HOUGH_GRADIENT, 4, thresholded.height() / 4,
							500, 50, 0, 0);
					int rows = circles.rows();
					int elemSize = (int) circles.elemSize();
					float[] data2 = new float[rows * elemSize / 4];

					// stocking bounds of the goals and the play ground
					List<Integer> bordsG1 = goal1.bords(videoWidth/resDivider, videoHeight/resDivider);
					List<Integer> bordsG2 = goal2.bords(videoWidth/resDivider, videoHeight/resDivider);
					List<Integer> bordsT = terrain.bords(videoWidth/resDivider, videoHeight/resDivider);

					// define the color of the goals at the max we detect : red
					Scalar couleurBut1 = hsv_max;
					Scalar couleurBut2 = hsv_max;

					if (data2.length > 0) {
						circles.get(0, 0, data2);
						Rect r = detect_ball(thresholded);
						if (r != null) {
							// detect the match moves
							double x = (r.tl().x + r.br().x) / 2;
							double y = (r.tl().y + r.br().y) / 2;
							Point pt = new Point(x, y);
							int action = detectAction(x, y);
							if (action == 1 || (animBut1 > 0 && animBut1 < getFrame() - 60)) {
								if (animBut1 <= 0) {
									animBut1 = (int) getFrame();
								}
								couleurBut1 = new Scalar(255, 0, 0);
							} else {
								animBut1 = -1;
							}
							if (action == 2 || (animBut2 > 0 && animBut2 > getFrame() - 60)) {
								if (animBut2 <= 0) {
									animBut2 = (int) getFrame();
								}
								couleurBut2 = new Scalar(255, 0, 0);
							} else {
								animBut2 = -1;
							}
							Imgproc.circle(frame, pt, 20, new Scalar(38, 255, 255), 5);
							// to show the coos of the ball directly on the image returned
						} else {
							detectAction(-1, -1);
						}
					}

					// if the goals have been initialized
					if (goal1.isInit && goal2.isInit && terrain.isInit) {

						// add there limits of the goals and playground on the image
						Imgproc.rectangle(frame, new Rect(bordsG1.get(0), bordsG1.get(2),
								bordsG1.get(1) - bordsG1.get(0), bordsG1.get(3) - bordsG1.get(2)), couleurBut1);
						Imgproc.rectangle(frame, new Rect(bordsG2.get(0), bordsG2.get(2),
								bordsG2.get(1) - bordsG2.get(0), bordsG2.get(3) - bordsG2.get(2)), couleurBut2);
						Imgproc.rectangle(frame, new Rect(bordsT.get(0), bordsT.get(2), bordsT.get(1) - bordsT.get(0),
								bordsT.get(3) - bordsT.get(2)), hsv_max);
					}
				}
			} catch (Exception e) {
				// log the error
				System.err.println("Exception during the image elaboration: " + e);
			}
		} else {
			try {
				this.pause();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		try {
			updateSliderAndLabels();
			Thread.sleep(1);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return frame;
	}

	// update the slider at the current frame we are at and set the labels' text
	// accordingly
	public void updateSliderAndLabels() {
		Platform.runLater(new Runnable() {
			@Override
			public void run() {
				slider.setValue(getFrame());
				currentframetext.setText(format.format(getFrame()) + " /");
				currenttimetext.setText(format.format((getFrame() * 1 / fps) / 60) + ":"
						+ format.format((getFrame() * 1 / fps) % 60) + " /");
			}
		});
	}

	/**
	 * Stop the acquisition from the camera and release all the resources
	 */
	private synchronized void stopAcquisition() {
		if (this.future != null) {
			future.cancel(false);
		}
		if (this.capture.isOpened()) {
			// release the camera
			this.capture.release();
		}
	}

	// method that detect the ball from any color and return a rectangle that
	// encompasses the ball
	public static Rect detect_ball(Mat outmat) {

		Mat v = new Mat();
		List<MatOfPoint> contours = new ArrayList<MatOfPoint>();
		Imgproc.findContours(outmat, contours, v, Imgproc.RETR_LIST, Imgproc.CHAIN_APPROX_SIMPLE);

		double maxArea = -1;
		int maxAreaIdx = -1;
		Rect r = null;

		for (int idx = 0; idx < contours.size(); idx++) {
			Mat contour = contours.get(idx);
			double contourarea = Imgproc.contourArea(contour);
			if (contourarea > maxArea) {
				maxArea = contourarea;
				maxAreaIdx = idx;
				r = Imgproc.boundingRect(contours.get(maxAreaIdx));
			}
		}
		v.release();
		return r;

	}

	/**
	 * Update the {@link ImageView} in the JavaFX main thread
	 *
	 * @param view  the {@link ImageView} to update
	 * @param image the {@link Image} to show
	 */
	private void updateImageView(ImageView view, Image image) {
		Platform.setImplicitExit(false);
		Utils.onFXThread(view.imageProperty(), image);
	}

	// change the color of the ball we want to detect (sync)
	public void changeBallColor(double hmin_, double hmax_) {
		this.hsv_min = new Scalar(hmin_, 50, 50, 0);
		this.hsv_max = new Scalar(hmax_, 255, 255, 0);
		this.hsv_min2 = new Scalar(175, 50, 50, 0);
		this.hsv_max2 = new Scalar(179, 255, 255, 0);
	}

	public synchronized void seek(double numFrame) {
		eventsController.getmainController().pauseVid();
		capture.set(Videoio.CAP_PROP_POS_FRAMES, numFrame - 1);
		frameGrabber.run();
	}

	public synchronized double getFrame() {
		return this.capture.get(Videoio.CAP_PROP_POS_FRAMES);
	}

	// both next method are for the gesture of the properties file
	public String getFilename() {
		String filename = this.path.getFileName().toString();
		return filename.substring(0, filename.length() - 4);
	}

	public String getTruncatedPath() {
		return this.path.toString().substring(0, this.path.toString().length() - 4);
	}

	public synchronized double getTotalFrame() {
		return this.capture.get(Videoio.CAP_PROP_FRAME_COUNT);
	}

	// method that returns the fps ratio
	public double getFPS() {
		return this.fps;
	}

	// play or release the capture
	public synchronized void play() {
		this.future = this.timer.scheduleAtFixedRate(frameGrabber, 0, (1000 / fps), TimeUnit.MILLISECONDS);
//			this.timer.scheduleAtFixedRate(frameGrabber, 0, (1000/fps));;
	}

	// pause the capture
	public synchronized void pause() throws InterruptedException {
		if (this.future != null) {
			this.future.cancel(false);
		}
//			this.timer = Executors.newSingleThreadScheduledExecutor();
	}

	// stop or release the fact that we are counting goals
	// e.j: players are stopping for a while without stopping recording
	public void toggleAnalysis() {
		this.pauseAnalysis = !this.pauseAnalysis;
	}

	/**
	 * On application close, stop the acquisition from the camera
	 */
	public void setClosed() {
		this.stopAcquisition();
	}

	// method that handle every type of action :
	// - ball exiting the playground
	// - goal
	// - gamelle
	public int detectAction(double x, double y) {
		List<Integer> bordsG1 = goal1.bords(videoWidth/resDivider, videoHeight/resDivider);
		List<Integer> bordsG2 = goal2.bords(videoWidth/resDivider, videoHeight/resDivider);
		List<Integer> bordsT = terrain.bords(videoWidth/resDivider, videoHeight/resDivider);

		if (goal1.isInit && goal2.isInit && terrain.isInit) {

			// lastMvm and lastMvm2 are like: {atGoal1, aroundGoal1, atGoal2, aroundGoal2,
			// ...}

			int voisinage = 30;

			if ((x > bordsG1.get(0) - voisinage) && (x < bordsG1.get(1) + voisinage) && (y > bordsG1.get(2) - voisinage)
					&& (y < bordsG1.get(3) + voisinage)) {
				// ball is around the right goal zone
				if (x > bordsG1.get(0) && x < bordsG1.get(1) && y > bordsG1.get(2) && y < bordsG1.get(3)) {
					// ball is in the right goal zone
					lastMvm.add(true);
					lastMvm.add(false);
				} else {
					// ball close to the right goal zone
					lastMvm.add(false);
					lastMvm.add(true);
				}
			} else if ((x > bordsG2.get(0) - voisinage) && (x < bordsG2.get(1) + voisinage)
					&& (y > bordsG2.get(2) - voisinage) && (y < bordsG2.get(3) + voisinage)) {
				// ball is around the right goal zone
				if (x > bordsG2.get(0) && x < bordsG2.get(1) && y > bordsG2.get(2) && y < bordsG2.get(3)) {
					// ball is in the right goal zone
					lastMvm2.add(true);
					lastMvm2.add(false);
				} else {
					// ball close to the right goal zone
					lastMvm2.add(false);
					lastMvm2.add(true);
				}
			} else {
				// ball in the goal zone
				lastMvm.add(false);
				lastMvm.add(false);

				lastMvm2.add(false);
				lastMvm2.add(false);
			}

			// detect gamelles
			int n1 = lastMvm.size();
			int n2 = lastMvm2.size();
			if (n1 >= 4) {
				if (!lastMvm.get(n1 - 1) && !lastMvm.get(n1 - 2) && !lastMvm.get(n1 - 3) && lastMvm.get(n1 - 4)) {
					// detect left goals
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								eventsController.addEvent((int) getFrame(), 1, 1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
					fieldExit = true;
					return 1;
				} else if (lastMvm.get(n1 - 1) && !lastMvm.get(n1 - 2) && !lastMvm.get(n1 - 3) && lastMvm.get(n1 - 4)) {
					// detect left gamelles
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								eventsController.addEvent((int) getFrame(), 3, 1);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
					fieldExit = true;
					return -1;
				} else if (!lastMvm2.get(n2 - 1) && !lastMvm2.get(n2 - 2) && !lastMvm2.get(n2 - 3)
						&& lastMvm2.get(n2 - 4)) {
					// detect right goals
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								eventsController.addEvent((int) getFrame(), 1, 0);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
					fieldExit = true;
					return 2;
				} else if (lastMvm2.get(n2 - 1) && !lastMvm2.get(n2 - 2) && !lastMvm2.get(n2 - 3)
						&& lastMvm2.get(n2 - 4)) {
					// detect right gamelles
					Platform.runLater(new Runnable() {
						@Override
						public void run() {
							try {
								eventsController.addEvent((int) getFrame(), 3, 0);
							} catch (InterruptedException e) {
								e.printStackTrace();
							}
						}
					});
					fieldExit = true;
					return -2;
				} else {
					// detect exits
					if ((x + y >= 0)
							&& (x < bordsT.get(0) || x > bordsT.get(1) || y < bordsT.get(2) || y > bordsT.get(3))) {
						if (!fieldExit) {
							fieldExit = true;
							// if it exits in the middle right part of the playground
							if (x > (bordsT.get(0) + bordsT.get(1)) / 2) {
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										try {
											eventsController.addSortie((int) getFrame(), 0);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								});
							} else {
								Platform.runLater(new Runnable() {
									@Override
									public void run() {
										try {
											eventsController.addSortie((int) getFrame(), 1);
										} catch (InterruptedException e) {
											e.printStackTrace();
										}
									}
								});
							}
						}
					} else {
						if (x + y > 0) {
							fieldExit = false;
						}
					}
					return 0;
				}
			} else {
				return -4;
			}
		} else {
			return -4;
		}
	}
}
