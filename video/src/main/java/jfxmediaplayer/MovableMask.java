package jfxmediaplayer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javafx.beans.binding.DoubleBinding;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.EventHandler;
import javafx.geometry.Bounds;
import javafx.scene.Cursor;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;

// used to record the position shift when dragging
class Delta {
	Double x = 0.;
	Double y = 0.;
}

class MovableCircle extends Circle {
	private DoubleBinding x;
	private DoubleBinding y;

	MovableCircle() {
		x = this.layoutXProperty().add(this.getCenterX());
		y = this.layoutYProperty().add(this.getCenterY());
	}

	public DoubleBinding getX() {
		return x;
	}

	public DoubleBinding getY() {
		return y;
	}
}

public class MovableMask extends Rectangle {
	private List<MovableCircle> c; // The corners of the mask
	private int initX;
	private int initY;
	private int initW;
	private int initH;
	private Color color;
	public boolean isInit = false;
	private boolean hidden = false;
	private boolean sizeSet;
	private ImageView video;

	MovableMask(int x, int y, int w, int h, Color color) {
		initX = x;
		initY = y;
		initW = w;
		initH = h;
		this.color = color;
	}

	void init(ImageView video) {
		if (!isInit) { // Only initialize once
			this.video = video;
			BorderPane root = ((BorderPane) video.getScene().getRoot());
			Bounds border = video.getBoundsInParent();
			c = new ArrayList<MovableCircle>();
			while (c.size() < 4)
				c.add(new MovableCircle());
			this.setFill(color);
			// transform input color to opaque for the handles at the corners
			Color colorOpaque = new Color(color.getRed(), color.getGreen(), color.getBlue(), 1);
			double ratio = border.getWidth()/550;
			// Initialize top left corner
			c.get(0).setFill(colorOpaque);
			c.get(0).setLayoutX((initX*ratio + border.getMinX()));
			c.get(0).setLayoutY((initY*ratio + border.getMinY()));
			c.get(0).setRadius(5);

			// Initialize bottom right corner
			c.get(3).setFill(colorOpaque);
			c.get(3).setLayoutX(((initX+ initW)*ratio  + border.getMinX()));
			c.get(3).setLayoutY(((initY + initH)*ratio  + border.getMinY()));
			c.get(3).setRadius(5);

			// Initialize top right corner using previous handles
			c.get(1).setFill(colorOpaque);
			c.get(1).centerXProperty().bind(c.get(3).getX());
			c.get(1).centerYProperty().bind(c.get(0).getY());
			c.get(1).setRadius(5);

			// Initialize bottom left corner using previous handles
			c.get(2).setFill(colorOpaque);
			c.get(2).centerXProperty().bind(c.get(0).getX());
			c.get(2).centerYProperty().bind(c.get(3).getY());
			c.get(2).setRadius(5);

			// Bidirectional bind to make each corner follow the others
			c.get(0).centerXProperty().bind(c.get(2).getX());
			c.get(0).centerYProperty().bind(c.get(1).getY());
			c.get(3).centerXProperty().bind(c.get(1).getX());
			c.get(3).centerYProperty().bind(c.get(2).getY());

			// Bind the rectangle to the handles
			this.layoutXProperty().bind(c.get(0).getX().add(c.get(2).layoutXProperty()));
			this.layoutYProperty().bind(c.get(0).getY().add(c.get(1).layoutYProperty()));
			this.widthProperty().bind(c.get(3).layoutXProperty().subtract(c.get(0).layoutXProperty())
					.add(c.get(3).centerXProperty()).subtract(c.get(0).centerXProperty()));
			this.heightProperty().bind(c.get(3).layoutYProperty().subtract(c.get(0).layoutYProperty())
					.add(c.get(3).centerYProperty()).subtract(c.get(0).centerYProperty()));

			// Add the rectangle (this extends rectangle) to the scene
			if (!root.getChildren().contains(this)) {
				root.getChildren().add(this);
			}

			// Add each circle to the scene
			for (int i = 0; i < c.size(); i++)
				root.getChildren().add(c.get(i));

			// Drag of circle handles
			for (int i = 0; i < c.size(); i++) {
				final Circle circle = c.get(i);
				setMovable(circle);
			}

			// Drag of whole mask
			final Delta maskDelta = new Delta();
			final Delta tailleRect = new Delta();
			this.setOnMousePressed(mouseEvent -> {
				// record a delta distance for the drag and drop operation.
				maskDelta.x = this.c.get(0).getX().doubleValue() - mouseEvent.getSceneX();
				maskDelta.y = this.c.get(0).getY().doubleValue() - mouseEvent.getSceneY();
				this.setCursor(Cursor.MOVE);
				if (!sizeSet) {
					sizeSet = true;
					tailleRect.x = this.c.get(3).getX().subtract(this.c.get(0).getX()).doubleValue();
					tailleRect.y = this.c.get(3).getY().subtract(this.c.get(0).getY()).doubleValue();
				}
			});
			this.setOnMouseReleased(mouseEvent -> {
				this.setCursor(Cursor.HAND);
				sizeSet = false;
			});
			this.setOnMouseDragged(mouseEvent -> {
				c.get(0).setLayoutX(mouseEvent.getSceneX() + maskDelta.x);
				c.get(0).setLayoutY(mouseEvent.getSceneY() + maskDelta.y);

				c.get(3).setLayoutX(mouseEvent.getSceneX() + maskDelta.x + tailleRect.x);
				c.get(3).setLayoutY(mouseEvent.getSceneY() + maskDelta.y + tailleRect.y);
			});
			this.setOnMouseEntered(mouseEvent -> {
				this.setCursor(Cursor.HAND);
			});

			// On resize of image make the masks follow the magnification
			video.boundsInParentProperty().addListener(new ChangeListener<Bounds>() {
				@Override
				public void changed(ObservableValue<? extends Bounds> ov, Bounds oldBounds, Bounds bounds) {
					c.get(3).setLayoutX(bounds.getWidth() / oldBounds.getWidth()
							* (c.get(3).getX().getValue() - oldBounds.getMinX()) + bounds.getMinX());
					c.get(3).setLayoutY(bounds.getWidth() / oldBounds.getWidth()
							* (c.get(3).getY().getValue() - oldBounds.getMinY()) + bounds.getMinY());
					c.get(0).setLayoutX(bounds.getWidth() / oldBounds.getWidth()
							* (c.get(0).getX().getValue() - oldBounds.getMinX()) + bounds.getMinX());
					c.get(0).setLayoutY(bounds.getWidth() / oldBounds.getWidth()
							* (c.get(0).getY().getValue() - oldBounds.getMinY()) + bounds.getMinY());

				}
			});
			isInit = true;

		}

	}

	void setMovable(Shape shape) {
		final Delta dragDelta = new Delta();
		shape.setOnMousePressed(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				// record a delta distance for the drag and drop operation.
				dragDelta.x = shape.getLayoutX() - mouseEvent.getSceneX();
				dragDelta.y = shape.getLayoutY() - mouseEvent.getSceneY();
				shape.setCursor(Cursor.MOVE);
			}
		});
		shape.setOnMouseReleased(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				shape.setCursor(Cursor.HAND);
			}
		});
		shape.setOnMouseDragged(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				shape.setLayoutX(mouseEvent.getSceneX() + dragDelta.x);
				shape.setLayoutY(mouseEvent.getSceneY() + dragDelta.y);
			}
		});
		shape.setOnMouseEntered(new EventHandler<MouseEvent>() {
			@Override
			public void handle(MouseEvent mouseEvent) {
				shape.setCursor(Cursor.HAND);
			}
		});
	}

	// Resets the mask to its initial position
	void reset() {
		c.get(0).setLayoutX(initX);
		c.get(0).setLayoutY(initY);
		c.get(3).setLayoutX(initX + initW);
		c.get(3).setLayoutY(initX + initH);
	}

	// Hide or show the mask
	void toggleVisibility() {
		this.setVisible(hidden);
		c.forEach((circle) -> {
			circle.setVisible(hidden);
		});
		hidden = !hidden;
	}

	// returns borders of the mask in the coordinates of the ImageView. in order:
	// left right top bottom
	@SuppressWarnings({ "rawtypes", "unchecked" })
	List<Integer> bords(double videoW, double videoH) {
		if (video == null) {
			return new ArrayList();
		}
		// Bounds of the ImageView in the window
		Bounds bounds = video.getBoundsInParent();

		// Offset of the top left of the ImageView from the top left of the window
		int offsetY = (int) -bounds.getMinY();
		int offsetX = (int) -bounds.getMinX();

		// Ratio of visible size over original video size
		double ratioX = (bounds.getWidth() / videoW);
		double ratioY = (bounds.getHeight() / videoH);

		MovableCircle coinHG = c.get(0);// top left corner
		MovableCircle coinBD = c.get(3);// bottom right corner

		// Change the reference from the Scene/Stage back to ImageView
		int gauche = (int) (((coinHG.getLayoutX() + coinHG.getCenterX() + offsetX) / ratioX) );
		int droite = (int) (((coinBD.getLayoutX() + coinBD.getCenterX() + offsetX) / ratioX));
		int haut = (int) (((coinHG.getLayoutY() + coinHG.getCenterY() + offsetY) / ratioY));
		int bas = (int) (((coinBD.getLayoutY() + coinBD.getCenterY() + offsetY) / ratioY));
		return Arrays.asList(gauche, droite, haut, bas);
	}
}
