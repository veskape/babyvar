package jfxmediaplayer;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.Set;

public class SaveProcess {

	private String ppath;
	private final Properties configProp = new Properties();

	SaveProcess(String path) {
		InputStream input = null;

		// set the path to the future properties file
		this.ppath = path + ".properties";

		// if the path exists create an input stream and read it
		if (Files.exists(Paths.get(ppath))) {
			try {
				input = new FileInputStream(ppath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		// if it does not exist create it (flush())
		else {
			setProperty("path", ppath);
			try {
				flush();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
			try {
				input = new FileInputStream(ppath);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
		System.out.println("Reading all properties from the file");
		try {
			// read the properties written in the properties file
			configProp.load(input);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getProperty(String key) {
		return configProp.getProperty(key);
	}

	// recover all the property names : goal etc...
	public Set<String> getAllPropertyNames() {
		return configProp.stringPropertyNames();
	}

	// return true if the file contains a specific property key
	public boolean containsKey(String key) {
		return configProp.containsKey(key);
	}

	// set the property that has the specified key
	public void setProperty(String key, String value) {
		configProp.setProperty(key, value);
	}

	// create/update the .properties file
	public void flush() throws FileNotFoundException, IOException {
		try (final OutputStream output = new FileOutputStream(ppath);) {
			configProp.store(output, "File Updated");
			output.close();
		}
	}
	
	public String getFilePath() {
		return ppath;
	}

}