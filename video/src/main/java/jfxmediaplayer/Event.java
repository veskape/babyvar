package jfxmediaplayer;

public class Event {
	private String team_; // attribute that stock the goal place
	private Integer frame_; // the frame the action went on
	private Integer action_; // the type of action 1:goal 2:exit 3:gamelle

	public Event(Integer action, Integer frame, String team) {
		frame_ = frame;
		team_ = team;
		action_ = action;
	}

	public String toString() {
		return action_ + "," + frame_ + "," + team_;
	}

	public String getTeam_() {
		return team_;
	}

	public void setTeam_(String team_) {
		this.team_ = team_;
	}

	public Integer getFrame_() {
		return frame_;
	}

	public void setFrame_(Integer frame_) {
		this.frame_ = frame_;
	}

	public Integer getAction_() {
		return action_;
	}

	public void setAction_(Integer action_) {
		this.action_ = action_;
	}
}
