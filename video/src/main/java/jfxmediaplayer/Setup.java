package jfxmediaplayer;

import java.util.Locale;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

//not used as main because it extends Application
public class Setup extends Application {
	@Override
	public void start(Stage primaryStage) throws Exception {
		// on creation, we load fxml samples for our multiple windows and we set icons
		// for the whole project
		FXMLLoader loader = new FXMLLoader(getClass().getResource("sample.fxml"));
		BorderPane root = (BorderPane) loader.load();
		primaryStage.setTitle("BabyVAR");
		Scene scene = new Scene(root, 1500, 700);
		scene.getStylesheets().add(getClass().getResource("style.css").toExternalForm());
		primaryStage.setScene(scene);
		primaryStage.setMinWidth(900);
		primaryStage.setMinHeight(660);
		primaryStage.getIcons().add(new Image(getClass().getResource("icon-app.png").toString()));
		primaryStage.show();

		// loading the help user interface
		FXMLLoader loader3 = new FXMLLoader(getClass().getResource("help.fxml"));
		Parent root3;
		Stage stage3 = new Stage();

		root3 = loader3.load();
		stage3.setTitle("Help");
		stage3.setScene(new Scene(root3, 450, 450));

		Controller controller = loader.getController();
		controller.setHelpStage(stage3);
		primaryStage.setOnCloseRequest((new EventHandler<WindowEvent>() {
			public void handle(WindowEvent we) {
				// handle the closing : delete
				if (controller.getVideo() != null) {
					controller.getVideo().setClosed();
				}
				Platform.exit();
				System.exit(0);
			}
		}));
	}

	public static void main(String[] args) {

		// choice has been made that the project will be in English
		Locale.setDefault(Locale.ENGLISH);

		// windows library for OpenCV
		nu.pattern.OpenCV.loadLocally();

		// Linux libraries for OpenCV
		// System.load("/opt/brew/Cellar/opencv/4.5.4/share/java/opencv4/libopencv_java454.so");
		// System.load("/home/linuxbrew/.linuxbrew/Cellar/opencv/4.5.3_3/share/java/opencv4/libopencv_java453.so");

		// Application .launch() method
		launch(args);
	}
}
